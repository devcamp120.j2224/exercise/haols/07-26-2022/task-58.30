package com.devcamp.s50.task5830.customerlistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerListApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerListApiApplication.class, args);
	}

}
