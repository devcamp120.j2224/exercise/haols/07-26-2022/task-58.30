package com.devcamp.s50.task5830.customerlistapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5830.customerlistapi.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {

}
